package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/IncSW/geoip2"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type geoIPInfo struct {
	City               string  `json:"city"`
	CityGeoNameID      uint32  `json:"city_geoname_id"`
	Country            string  `json:"country"`
	CountryGeoNameID   uint32  `json:"country_geoname_id"`
	CountryCode        string  `json:"country_code"`
	Continent          string  `json:"continent"`
	ContinentCode      string  `json:"continent_code"`
	ContinentGeoNameID uint32  `json:"continent_geoname_id"`
	PostalCode         string  `json:"postal_code"`
	Latitude           float64 `json:"latitude"`
	Longitude          float64 `json:"longitude"`
	TimeZone           string  `json:"time_zone"`
}

// Encrypt string to AES CBC
func encrypt(plaintext string, key string, iv string, blockSize int) []byte {
	bKey := []byte(key)
	bIV := []byte(iv)
	bPlaintext := PKCS5Padding([]byte(plaintext), blockSize, len(plaintext))
	block, _ := aes.NewCipher(bKey)
	ciphertext := make([]byte, len(bPlaintext))
	mode := cipher.NewCBCEncrypter(block, bIV)
	mode.CryptBlocks(ciphertext, bPlaintext)
	return []byte(ciphertext)
}

// PKCS5Padding adds space to []byte slice
func PKCS5Padding(ciphertext []byte, blockSize int, after int) []byte {
	padding := (blockSize - len(ciphertext)%blockSize)
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func locate(ipAddr, lang string) *geoIPInfo {
	viper.SetDefault("mmdbPath", "./GeoLite2-City.mmdb")
	reader, err := geoip2.NewCityReaderFromFile(viper.GetString("mmdbPath"))
	if err != nil {
		panic(err)
	}
	record, err := reader.Lookup(net.ParseIP(ipAddr))
	if err != nil {
		// if IP not fount return empty struct
		return &geoIPInfo{}
	}

	res := &geoIPInfo{
		City:               record.City.Names[lang],
		CityGeoNameID:      record.City.GeoNameID,
		Country:            record.Country.Names[lang],
		CountryGeoNameID:   record.Country.GeoNameID,
		CountryCode:        record.Country.ISOCode,
		Continent:          record.Continent.Names[lang],
		ContinentCode:      record.Continent.Code,
		ContinentGeoNameID: record.Continent.GeoNameID,
		PostalCode:         record.Postal.Code,
		Latitude:           record.Location.Latitude,
		Longitude:          record.Location.Longitude,
		TimeZone:           record.Location.TimeZone,
	}
	return res
}

// SetupRouter set devault vars
func SetupRouter() *gin.Engine {
	// To add a default value :
	viper.SetDefault("ENV", "DEV")
	viper.SetDefault("port", "8080")

	if strings.ToLower(os.Getenv("ENVIRONMENT")) == "dev" || strings.ToLower(viper.GetString("env")) == "dev" {
		viper.SetConfigName("config")
		viper.SetConfigType("toml")
		viper.AddConfigPath(filepath.Dir("./"))
		viper.ReadInConfig()
	} else {
		viper.AutomaticEnv()
	}

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Hi there :)")
	})

	r.GET("/myip", func(c *gin.Context) {
		var lang string
		if c.Query("lang") == "" {
			lang = "en"
		}
		req := locate(c.ClientIP(), lang)
		jsonResp, _ := json.Marshal(req)
		c.String(http.StatusOK, base64.StdEncoding.EncodeToString(encrypt(string(jsonResp), viper.GetString("key"), viper.GetString("iv"), aes.BlockSize)))
	})

	r.GET("/timestamp", func(c *gin.Context) {
		c.String(http.StatusOK, strconv.FormatInt(time.Now().Unix(), 10))
	})

	return r
}

func main() {
	SetupRouter().Run(":" + viper.GetString("port"))
}
