package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestMyIP(t *testing.T) {
	// Expected body, it will empty json after decryption
	expected := "gPrKgBDtQ4wIFeyL3xyBJJfDKe/FRHEkod9gka90FAIhF2kritWvKEywbvAjTjhmBJ/x8jWmLaBx+kW+FD75UZ4HPa69ddfVQA+5Y+7SEGQVDc8o1fezC233ib/UbLXLFHuTFAuUWAtNgYRT7nnC95i7dWObYS6UeiTo8CXEn3E8Vty+YEB5oPGRIvU+JEnkovSujF7jhqtdScXo4XthgzabTVw4rOlCh16g7+/DUer+mt51jo1KYS2g2J6aWaZmDpPDpAM6412hxvQnPfNo5w=="
	// Grab our router
	router := SetupRouter()
	// Perform a GET request with that handler.
	w := performRequest(router, "GET", "/myip")
	// Assert we encoded correctly,
	// the request gives a 200
	assert.Equal(t, http.StatusOK, w.Code)

	assert.Equal(t, expected, w.Body.String())
}
