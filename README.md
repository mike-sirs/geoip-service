[![pipeline status](https://gitlab.com/mike-sirs/geoip-service/badges/master/pipeline.svg)](https://gitlab.com/mike-sirs/geoip-service/-/commits/master)
[![coverage report](https://gitlab.com/mike-sirs/geoip-service/badges/master/coverage.svg)](https://gitlab.com/mike-sirs/geoip-service/-/commits/master)

# The project uses MaxMind lite or full database to provide `AES:CBC` encrypted feed with geo data.

### Decrypted responce.
```JSON
{
   "city":"Minsk",
   "city_geoname_id":625144,
   "country":"Belarus",
   "country_geoname_id":630336,
   "country_code":"BY",
   "continent":"Europe",
   "continent_code":"EU",
   "continent_geoname_id":6255148,
   "postal_code":"220017",
   "latitude":53.9,
   "longitude":27.5667,
   "time_zone":"Europe/Minsk"
}
```

- AES key and init vector can be set in `config.toml` for dev env or using OS ENV for any other environment. 
- Default path to mmdb file and its name is `./GeoLite2-City.mmdb`.

### Endpoints
| Path | Description |
|---|---|
|/         |  Just hi there =)|
|/myip     |  AES encrypted base64 feed|
|/timestamp|  Unix timestamp|

