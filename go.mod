go 1.15

module gitlab.com/mike-sirs/geoip-service

require (
	github.com/IncSW/geoip2 v0.1.0
	github.com/gin-gonic/gin v1.6.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0
)
